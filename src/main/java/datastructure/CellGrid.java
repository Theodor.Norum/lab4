package datastructure;


import cellular.CellState;

public class CellGrid implements IGrid {

    int rows;
    int cols;
    CellState[][] grid;
    


    public CellGrid(int rows, int columns, CellState initialState) {
		this.rows = rows;
        this.cols = columns;
        grid = new CellState[rows][columns];
	}
    
    @Override
    public int numRows() {
        return rows;
    }

    @Override
    public int numColumns() {
        return cols;
    }

    @Override
    public void set(int row, int column, CellState element) {
        try {
            grid[row][column] = element;
        }
        catch(IllegalArgumentException e) {
            ;
        }
    }

    @Override
    public CellState get(int row, int column) {
        try {
            ;
        }
        catch(IllegalArgumentException e) {
            ;
        }
        return grid[row][column];
    }

    @Override
    public IGrid copy() {
        IGrid gridCopy = new CellGrid(this.rows,this.cols, null);
        for (int x = 0; x < cols; x++)
			for(int y = 0; y < rows; y++)
				gridCopy.set(x,  y,  get(x, y));
        return gridCopy;

    }
    
}
