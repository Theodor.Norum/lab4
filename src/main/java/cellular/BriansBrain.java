package cellular;

import java.util.Random;

import datastructure.CellGrid;
import datastructure.IGrid;



public class BriansBrain implements CellAutomaton {
    /**
	 * The grid of cells
	 */
	IGrid currentGeneration;

	int rows;
	int cols;
	/**
	 * 
	 * Construct a Game Of Life Cell Automaton that holds cells in a grid of the
	 * provided size
	 * 
	 * @param height The height of the grid of cells
	 * @param width  The width of the grid of cells
	 */
	public BriansBrain(int rows, int columns) {
		this.rows = rows;
		this.cols = columns;
		currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
		initializeCells();
	}

	@Override
	public void initializeCells() {
		Random random = new Random();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				if (random.nextBoolean()) {
					currentGeneration.set(row, col, CellState.ALIVE);
				} else {
					currentGeneration.set(row, col, CellState.DEAD);
				}
			}
		}
	}

	@Override
	public int numberOfRows() {
		return rows;
	}

	@Override
	public int numberOfColumns() {
		return cols;
	}

	@Override
	public CellState getCellState(int row, int col) {
		return currentGeneration.get(row, col);
	}

	@Override
	public void step() {
		IGrid nextGeneration = currentGeneration.copy();
		for (int x = 0; x < numberOfColumns(); x++) {
			for (int i = 0; i < numberOfRows(); i++) {
				nextGeneration.set(x, i, getNextCell(x, i));
			}
		}
		currentGeneration = nextGeneration;
	}

	@Override
	public CellState getNextCell(int row, int col) {
		if (getCellState(row, col) == CellState.ALIVE)
		{
			return CellState.DYING;
		}

		else if (getCellState(row, col) == CellState.DYING) {
			return CellState.DEAD;
		}

		else if (getCellState(row, col) == CellState.DEAD && countNeighbors(row, col, CellState.ALIVE) == 2) {
			return CellState.ALIVE;
		}
		else {
			return CellState.DEAD;
		}
	}

	/**
	 * Calculates the number of neighbors having a given CellState of a cell on
	 * position (row, col) on the board
	 * 
	 * Note that a cell has 8 neighbors in total, of which any number between 0 and
	 * 8 can be the given CellState. The exception are cells along the boarders of
	 * the board: these cells have anywhere between 3 neighbors (in the case of a
	 * corner-cell) and 5 neighbors in total.
	 * 
	 * @param x     the x-position of the cell
	 * @param y     the y-position of the cell
	 * @param state the Cellstate we want to count occurences of.
	 * @return the number of neighbors with given state
	 */
	private int countNeighbors(int row, int col, CellState state) {
		int count = 0;
		for (int x = -1; x <= 1; x++) {
			for (int y = -1; y <= 1; y++) {
				if (x==0 && y==0) {
					continue;
				}
				if (row+x < 0) {
					continue;
				}
				if (col+y < 0) {
					continue;
				}
				if (row+x >= currentGeneration.numRows()) {
					continue;
				}
				if (col+y >= currentGeneration.numColumns()) {
					continue;
				}

			if (currentGeneration.get(row+x, col +y) == CellState.ALIVE) {
				count++;
			}
		}
	}
	return count;
	}

	@Override
	public IGrid getGrid() {
		return currentGeneration;
	}
}


